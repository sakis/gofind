# gofind

Simple tool to find text in files

# Warning

I have no idea what I'm doing.
This is my attempt to learn go.

What can go wrong?


# Things that I want to do (personal roadmap)

1. Search for a simple word in all the files in a directory, non recursively (iteration 0)
1. Recursive search with a flag (iteration 1)
1. Recursive support by default, with tests (v 0.0.1)


# Build and run


Install go and then either `make` or `go build; ./gofind <word>`
