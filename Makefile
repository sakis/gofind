.DEFAULT_GOAL := run

.PHONY:
fmt:
	@go fmt

.PHONY:
build: fmt
	@go build

.PHONY:
run: build
	@./gofind
