package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"sync"
)

func getFileList(path string) []aFile {
	var files []aFile

	filenames, err := ioutil.ReadDir(path)

	if err != nil {
		log.Fatal(err)
	}

	for _, item := range filenames {
		fullpath := filepath.Join(path, item.Name())

		if item.Mode().IsRegular() {
			aFile := aFile{basePath: path, filename: item.Name(), fullPath: fullpath}
			files = append(files, aFile)
		} else if item.IsDir() {
			if item.Name() == ".git" {
				continue
			}
			for _, file := range getFileList(fullpath) {
				files = append(files, file)
			}
		}
	}

	return files
}

func handleFile(file aFile, pattern *regexp.Regexp, waitGroup *sync.WaitGroup) {
	defer waitGroup.Done()
	file.loadContents()
	if !file.checkIsBinary() {
		file.matchContents(pattern)
	}
}

func main() {
	dirpath := "."
	needle := ""
	exitCode := 0

	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	argsLength := len(os.Args)
	if argsLength == 1 {
		fmt.Printf("Usage: %s <search term> [path]\n", os.Args[0])
		os.Exit(exitCode)
	}

	if argsLength >= 2 {
		needle = os.Args[1]
	}

	if argsLength >= 3 {
		dirpath = os.Args[2]
	}

	needle = fmt.Sprintf("(?im)^.*%s.*$", needle)
	pattern, err := regexp.Compile(needle)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	dirpath, _ = filepath.Abs(dirpath)
	waitGroup := new(sync.WaitGroup)
	for _, file := range getFileList(dirpath) {
		waitGroup.Add(1)
		go handleFile(file, pattern, waitGroup)
	}

	waitGroup.Wait()
	os.Exit(exitCode)
}
