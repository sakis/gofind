package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strings"
)

const (
	Green = "\033[1;32m%s\033[0m"
)

type aFile struct {
	contents []byte
	isBinary bool

	basePath string
	filename string
	fullPath string
}

func (file *aFile) loadContents() {
	contents, err := ioutil.ReadFile(file.fullPath)

	if err != nil {
		log.Fatal(err)
	}

	file.contents = contents
}

func (file *aFile) matchContents(pattern *regexp.Regexp) {
	matches := pattern.FindAll(file.contents, -1)
	if matches != nil {
		fmt.Printf("\n"+Green+"\n", file.fullPath)
		for _, match := range matches {
			match_str := string(match)
			fmt.Printf("%s\n", strings.TrimSpace(match_str))
		}
	}
}

func (file *aFile) checkIsBinary() bool {
	if len(file.contents) == 0 {
		file.isBinary = true
		return file.isBinary
	}

	contentsLength := len(file.contents)
	sliceSize := 1024
	if contentsLength < sliceSize {
		sliceSize = contentsLength
	}

	firstBytes := file.contents[0:sliceSize]
	zeroBytes := 0
	for _, item := range firstBytes {
		if item == 0 {
			zeroBytes++
		}
	}

	file.isBinary = zeroBytes > sliceSize/2

	return file.isBinary
}
